import React, { useState } from "react";
import styled from "styled-components";
import Layout from "../components/layout";
import { Waypoint } from "react-waypoint";

// import hb from "../images/hb.png";
// import Offbeat from "../images/Offbeat.png";

import { Intro, Process, Portfolio, Contact, Benefits } from "../sections/";

const IndexPage = () => {
  const [Section, setSection] = React.useState("Welcome");

  return (
    <Layout section={Section}>
      <div className="container">
        <Waypoint onEnter={() => setSection("Welcome")}>
          <Intro />
        </Waypoint>
        <Waypoint onEnter={() => setSection("Skills")}>
          <Process />
        </Waypoint>
        <Waypoint onEnter={() => setSection("Works")}>
          <Portfolio />
        </Waypoint>
        <Waypoint onEnter={() => setSection("Contact")}>
          <Contact />
        </Waypoint>

        <Waypoint onEnter={() => setSection("Contact")}>
          <Benefits />
        </Waypoint>
      </div>
    </Layout>
  );
};
const Row = styled.div`
  width: 100%;
  display: flex;
`;

const Column = styled.div`
  display: flex;
  min-height: 100px;
  background: "#5886b8";
  width: ${props => (props.s ? props.s + "%" : "auto")};
`;

const Welcome = styled.div`
  padding: 2rem 0;
  display: flex;
`;

export default IndexPage;
