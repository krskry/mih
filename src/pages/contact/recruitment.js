import React from 'react'
import  Layout  from '../../components/layout'
import SEO from '../../components/seo'

const NotFoundPage = () => (
  <Layout>
    <React.Fragment>
      <SEO title="not_found" />
        <h1>Recruitment</h1>
        <div style={{ width: "50vw" }}>
          <form>
            <label>name</label>
            <input TYPE="text" placeholder="email"></input>
            <label>email</label>
            <input TYPE="email" placeholder="phone"></input>
            <label>company name</label>
            <input type="tel" placeholder="phone"></input>
            <label>Comments</label>
            <textarea placeholder="email"></textarea>
          </form>
          <button>Send inquiry</button>
        </div>
    </React.Fragment>
  </Layout>
);

export default NotFoundPage;
