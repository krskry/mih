import React, { useState, useEffect } from "react";
import Layout from "../../components/layout";
import SEO from "../../components/seo";

const NotFoundPage = () => {
  const [name, setName] = useState("");
  const [lastName, setLastName] = useState("");
  const [phone, setPhone] = useState("");
  const [budget, setBudget] = useState("");
  const [comments, setComments] = useState("");
  useEffect(() => {
    // Zaktualizuj tytuł dokumentu korzystając z interfejsu API przeglądarki
  });
  return (
    <Layout>
      <React.Fragment>
        <SEO title="not_found" />
        <h1>Order Website</h1>
        <div style={{ width: "50vw" }}>
          <form>
            <label>name</label>
            <input
              onChange={e => setName(e.target.value)}
              type="text"
              minLength="5"
              required
              placeholder="email"
            ></input>
            <label>last name</label>
            <input
              onChange={e => setLastName(e.target.value)}
              type="text"
              minLength="5"
              required
              placeholder="email"
            ></input>
            <label>email</label>
            <input
              onChange={e => setPhone(e.target.value)}
              type="email"
              minLength="5"
              required
              placeholder="phone"
            ></input>
            <label>phone</label>
            <input
              onChange={e => setBudget(e.target.value)}
              type="tel"
              minLength="5"
              required
              placeholder="phone"
            ></input>
            <label> budget</label>
            <input
              onChange={e => setComments(e.target.value)}
              type="text"
              placeholder="budget"
            ></input>
            <label>Comments</label>
            <textarea placeholder="email"></textarea>
          </form>
          <button>Send inquiry</button>
        </div>
      </React.Fragment>
    </Layout>
  );
};

export default NotFoundPage;
