import React from "react";
import styled from "styled-components";

export function Process() {
  return (
    <div>
      <Row>
        <h2>Process</h2>
      </Row>
      <Row>
        <Column s={33}>
          <h2>1.Describe</h2>
        </Column>
        <Column s={33}>
          <h2>2.Design</h2>
        </Column>
        <Column s={33}>
          <h2>4.Build</h2>
        </Column>
        <Column s={33}>
          <h2>3.Publish</h2>
        </Column>
      </Row>
      <Row>
        <h2>Read more</h2>
      </Row>
    </div>
  );
}

const Row = styled.div`
  width: 100%;
  display: flex;
`;

const Column = styled.div`
  border: 2px solid black;
  h2 {
    margin: auto;
    color: black;
  }
  display: flex;
  margin: 10px;
  display: flex;
  min-height: 100px;

  width: ${props => (props.s ? props.s + "%" : "100%")};
`;
