import React from "react";
import Webdev from "../../images/webdev.svg";
import Graphics from "../../images/graphic.svg";
import uiux from "../../images/uiux.svg";
import styled from "styled-components";

export function Benefits() {
  return (
    <Skills>
      <div
      // style={{ color: Section === "Skills" && "Tomato" }}
      // className={Section === "Skills" ? "transition-fade" : ""}
      >
        <h2>Web development service for your next idea.</h2>{" "}
      </div>
      <div className="Gallery">
        <figure className="Gallery--Item   ">
          <div>
            <img className="skill" src={Webdev} alt={"sds"} />
          </div>
          <figcaption>Front-end development</figcaption>
          <figcaption>Building websites and interfaces</figcaption>
        </figure>
        <figure className="Gallery--Item  ">
          <div>
            <img className="skill" src={uiux} alt={"sds"} />
          </div>
          <figcaption>UI/UX design</figcaption>
          <figcaption>
            Video production company website with netlify cms integration
          </figcaption>
        </figure>
        <figure className="Gallery--Item  ">
          <div>
            <img className="skill" src={Graphics} alt={"sds"} />
          </div>
          <figcaption>Graphic design</figcaption>
          <figcaption>
            Creating a stunning and fast e-commerce sites with cms panel
            integration
          </figcaption>
        </figure>
      </div>
    </Skills>
  );
}
const Skills = styled.div`
  display: flex;
  flex-direction: column;

  .skill {
    height: 150px;
  }
`;
