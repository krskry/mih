import React from "react";

import { Link } from "gatsby";

export default function Foooter() {
  return (
    <footer
      id="footer"
      style={{
        margin: "auto",
        borderTop: "1px solid black",
        marginTop: 150,
        paddingBottom: 30
      }}
      className="site-foot"
    >
      <section>
        <br />
        <br />
        <dl className="alt">
          <dt>Email</dt>
          <dd>
            <a href="mailto:makingideashappen@protonmail.com">
              makingideashappen@protonmail.com
            </a>
          </dd>
          <dt>Phone</dt>
          <dd> +48 777 77 77</dd>
        </dl>{" "}
      </section>
      <section>
        <dt>Socials</dt>{" "}
        <dd>
          <a
            href="https://www.linkedin.com/in/kris-krysiak-3aa83297/"
            target="_blank"
          >
            <span> </span> LinkedIn
          </a>
        </dd>
        <dd>
          <a href="https://github.com/krs2000" target="_blank">
            <span className="label"> GitHub</span>
          </a>
        </dd>
      </section>
      <p>
        {" "}
        &copy; {new Date().getFullYear()}{" "}
        <Link to={`/`}>Making Ideas Happen</Link> &mdash; Making ideas happen
      </p>
    </footer>
  );
}
