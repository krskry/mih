import React from "react";
import PropTypes from "prop-types";
import { useStaticQuery, graphql } from "gatsby";
import Link from "gatsby-link";
import Header from "./header";
import Footer from "./footer";
import "../utils/css/screen.css";
// import  from "../images/logo.svg";
import styled from "styled-components";

const Wrapper = styled.section`
  header {
    max-width: 1200px;
  }
`;

const Layout = ({ children, section, title }) => {
  const data = useStaticQuery(graphql`
    query SiteTitleQuery {
      site {
        siteMetadata {
          title
        }
      }
    }
  `);

  // const { title, children, section } = props;
  const [toggleNav, setToggleNav] = React.useState(false);
  return (
    <div className={`site-wrapper ${toggleNav ? `site-head-open` : ``}`}>
      <main id="site-main" className="site-main">
        {children}
      </main>
      <Footer />
    </div>
  );
};

Layout.propTypes = {
  children: PropTypes.node.isRequired
};

export default Layout;
